<?php
 
  class ch_latest_posts extends WP_Widget{
      public function __construct(){
          $widget_ops = array(
               'classname' => 'widget_section',
               'description' => ' This widget is writed for showing latest posts.',
          );

          parent::__construct('ch_latest_posts' , '  Latest Posts Widget' , $widget_ops);
      }
      
      public function widget( $args , $instance ){ 
          $showposts = esc_attr($instance['input_number']); ?>
           <div class = 'widget_section wg3'>
                     <div class = 'widget_container'>
                           <span class = 'widget_title'><?php echo esc_attr($instance['w_widget_title_val']); ?></span>                                       
                              <ul class = 'widget_posts'>
                                 <?php if($showposts>0) : query_posts('showposts= ' . $showposts);  else: return true; endif;
                                      if ( have_posts() ) : while ( have_posts() ) : the_post();  ?>
                                       <li>
                                            <img src = '<?php if(has_post_thumbnail()) : echo the_post_thumbnail_url(); else : echo esc_attr($instance['post_thumb']) ; endif ; ?>' class = 'widget_post_image'/>
                                                 <span class = 'widget_textcontent'>
                                                   <a href = '#'><?php the_title() ; ?>!</a> 
                                                   <h6 class = 'widget_date'><?php echo get_the_date() ;?></h6>
                                                 </span>
                                                        
                                         </li>
                                 <?php endwhile ;  endif ; ?>
                              </ul><!-- Widget Posts-->
                     </div><!-- Widget Container-->
             </div><!-- Widget Section-->
      
    

      <style>
      /* latest posts */

.widget_title{
    display: block;
    color:white;
    font-family: raleway ;
    font-size:24px;
}

.widget_posts{
    width:100%;
    height:auto;
    display: block;
    list-style: none;
    padding: 0px;
}
/* UL-> Li Blocklarinin ayarlari */
.widget_posts >li {
    display: block;
    width:100%;
    height:80px;
    background:rgba(255, 255, 255, 0.055);
    margin: 0 auto;
    display: flex;
    align-items: center;
    margin-bottom:5px!important;
    overflow:hidden;
    border-radius: 3px;
 }
.widget_posts >li:hover{
    transition: 340ms;
    box-shadow:0px 0px 15px #191919a6;
    background:rgba(255, 255, 255, 0.089);
}
/* thubnail Ayarlari */
.widget_post_image{
    width:110px;
    height:80px;
    display: block;
    floaT:left;
}

.widget_textcontent{
    width:100%;
    height:auto;
    display: flex;
    flex-direction: column;
    padding:10px;
}
.widget_textcontent a , .widget_textcontent h6{display:  block; padding: 0px ; margin: 0px; }
.widget_textcontent a, .widget_textcontent h6{
    display: block;
    text-decoration: none;
    font-family: raleway ;
    font-size:13px;
    padding:  3px 0px ;
    color:rgb(230, 230, 230);
    opacity:.8;
}
.widget_textcontent a:hover  {opacity:1;  }
.widget_textcontent h6{font-size:11px; opacity:.8;}
      </style>
      <?php
      
    }                        
        

      public function form( $instance ){
           $post_number = esc_attr($instance['input_number']); 
             ?>
                    <label for ='<?php echo esc_attr($this->get_field_id('w_widget_title')); ?>'>Widget Title</label>
                    <input value = '<?php echo esc_attr($instance['w_widget_title_val']); ?>' type = 'text' name = '<?php echo esc_attr($this->get_field_name('w_widget_title_val')); ?>' id = '<?php echo esc_attr($this->get_field_id('w_widget_title')); ?>' min ='1' />
                
                    <br>
                    <label for ='<?php echo esc_attr($this->get_field_id('post_number')); ?>'>Post Number : </label>
                    <input value = '<?php echo esc_attr($post_number); ?>' type = 'number' name = '<?php echo esc_attr($this->get_field_name('input_number')); ?>' id = '<?php echo esc_attr($this->get_field_id('post_number')); ?>' min ='0' />
                    <br>

                    <label for = '<?php echo esc_attr($this->get_field_id('w_post_thumb')); ?>'>Default Image</label>
                    <input value = '<?php echo esc_attr($instance['post_thumb']); ?>' type = 'text' name = '<?php echo esc_attr($this->get_field_name('post_thumb')); ?>' id = '<?php echo esc_attr($this->get_field_id('w_post_thumb')); ?>' placeholder ='Url..' />

             <?php
 
      }

      public function update( $new_instance , $old_instance ){
         $old_instance['input_number'] = $new_instance['input_number'];
         $old_instance['post_thumb'] = $new_instance['post_thumb'];
         $old_instance['w_widget_title_val'] = $new_instance['w_widget_title_val'];
         return $old_instance;
      }
  }
  
  add_action('widgets_init' , function(){register_widget('ch_latest_posts');});

?>